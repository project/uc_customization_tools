Ubercart Customization Tools
============================

- Contains the following modules:

  * Ubercart Customization Tools - Cart Language
  * Ubercart Customization Tools - Disable product
  * Ubercart Customization Tools - Hide price
  * Ubercart Customization Tools - Multistep

  These can be independently turned on or off. Please refer to the
  README.txt files of the relevant module for further information.
