Ubercart Customization Tools - Cart Language
============================================

- Adds a Language column to the shopping cart table to indicate the language of
  a product.
