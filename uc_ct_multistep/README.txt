Ubercart Customization Tools - Multistep module
===============================================

- Makes a block available which shows the following three steps:

  1. Shopping cart (/cart)
  2. Checkout (/cart/checkout)
  3. Finished (/cart/checkout/complete)

  It adds a class to the list elements depending on whether it is the current step 
  and whether the step has already been completed or not.

- Disables title and description for Customer and Delivery panes in the checkout
  screen.

- Changes the wording of the Checkout button in the shopping cart from "Checkout"
  to "Next step".

- Changes the wording of the Cancel button in the Checkout screen from "Cancel"
  to "Previous step".

- Allows one to change the wording of the word "Products" in the headers of the
  shopping cart table (using drush vset uc_ct_string_products).

- Allows one to change the wording of the word "Quantity" on top of the "Add to
  cart" button (using drush vset uc_ct_string_quantity).

- Allows one to change the wording of the word "Continue shopping" in the
  shopping cart (using drush vset uc_ct_string_continue_shopping).

- Adds a "View cart contents" button next to the "Add to cart" button.

- Skips the "checkout review" step (such as in the uc_optional_checkout_review
  module).

- Removes the "Update cart" button and disables the quantity fields in the cart.
