Ubercart Customization Tools - Hide price module
================================================

- Hides price information in nodes.

- Hides price information in checkout table.

- For hiding price information in the confirmation email, please edit the
  relevant template.

- For hiding price fields in the node edit screen, please use the UC Product
  Power Tools module.
