Ubercart Customization Tools - Disable product
==============================================

- Allows flagging products as available or not and disabling the "Add to cart" /
  "Add to wishlist" buttons for unavailable products.

- Dependency: Flag module version 3.x

- In order to use this module, please set up the following permissions:

  * Flag Content entities as Disable "Add to cart" 
  * Unflag Content entities as Disable "Add to cart"
  * Flag Content entities as Disable wishlist
  * Unflag Content entities as Disable wishlist

  You can now mark products as disabled by going to the node addition/edition page
  and marking the relevant checkbox in the Flags tab.
